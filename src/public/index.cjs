// Number of matches played per year for all the years in IPL.

fetch("output/1-matches-per-year.json")
    .then((data) => data.json())
    .then((data) => {
        const matchesPerYearData = Object.values(data);


        Highcharts.chart('container1', {

            title: {
                text: 'Matches played per year for all the years in IPL.',
            },

            yAxis: {
                title: {
                    text: 'Total Matches'
                }
            },

            xAxis: {
                accessibility: {
                    rangeDescription: 'Range: 2008 to 2017'
                }
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2008
                }
            },

            series: [{
                name: 'Total matches',
                data: matchesPerYearData,
                colorByPoint: true,
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    });

// Number of matches won per team per year in IPL.

fetch("output/2-matches-won-per-team-per-year.json")
    .then((data) => data.json())
    .then((data) => {
        // console.log(data);

        const allTeams = Object.values(data)
            .reduce((teamsData, currentTeam) => {
                let teams = Object.keys(currentTeam)
                    .map((team) => {
                        return team;
                    })
                    .filter((team) => {
                        const teamNotInList = teamsData.find((currentTeam) => {
                            return currentTeam === team;
                        });
                        return teamNotInList === undefined;

                    });

                teamsData.push(...teams);

                return teamsData;
            }, []);

        let countSeason = 1;
        const matchesWonPerTeam = Object.values(data)
            .reduce((matchesWonTeams, currentTeamData) => {
                Object.entries(currentTeamData)
                    .map(([teamName, teamsData]) => {
                        const currentTeam = matchesWonTeams.find((currentTeamData) => {
                            return currentTeamData.name === teamName;
                        });
                        if (currentTeam === undefined) {
                            matchesWonTeams.push({
                                name: teamName,
                                data: [teamsData]
                            });
                        } else {
                            currentTeam.data.push(teamsData);
                        }
                    });
                allTeams.map((teamName) => {
                    const teamsData = matchesWonTeams.find((teamData) => {
                        return teamData.name === teamName;
                    });
                    if (teamsData === undefined) {
                        matchesWonTeams.push({
                            name: teamName,
                            data: [null],
                        });
                    } else {
                        if (teamsData.data.length !== countSeason) {
                            teamsData.data.push(null);
                        }
                    }
                });
                countSeason += 1;

                return matchesWonTeams;
            }, []);

        const yearAsCategories = Object.keys(data);

        // console.log(matchesWonPerTeam)

        Highcharts.chart('container2', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Matches won per team per year in IPL'
            },
            xAxis: {
                categories: yearAsCategories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Matches Won (per year)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.f} matches</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: matchesWonPerTeam
        });

    });

//Extra runs conceded per team in the year 2016

fetch("output/3-extra-runs-conceded-per-team-in-2016.json")
    .then((data) => data.json())
    .then((data) => {
        // console.log(data);

        const extraRunsConceded = Object.values(data);
        const teams = Object.keys(data);

        Highcharts.chart('container3', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Extra runs conceded per team in the year 2016'
            },
            xAxis: {
                categories: teams,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Extra runs conceded'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.f} runs</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Team',
                data: extraRunsConceded,
                colorByPoint: true,
            }],
        });
    });

// Top 10 economical bowlers in the year 2015

fetch("output/4-top-10-economical-bowlers-in-2015.json")
    .then((data) => data.json())
    .then((data) => {
        // console.log(data);

        const allBowlersEconomy = data.map((bowlerData) => {
            return parseFloat(bowlerData.economy);
        });

        const allBowlersName = data.map((bowlerData) => {
            return bowlerData.name;
        });

        Highcharts.chart('container4', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top 10 economical bowlers in the year 2015'
            },
            xAxis: {
                categories: allBowlersName,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: "Bowler's Economy"
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Economy ',
                data: allBowlersEconomy,
                colorByPoint: true,
            }]
        });

    });

// the number of times each team won the toss and also won the match

fetch("output/5-team-won-toss-also-won-match.json")
    .then((data) => data.json())
    .then((data) => {
        // console.log(data);

        const timesTeamWon = Object.values(data);
        const teams = Object.keys(data);

        Highcharts.chart('container5', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of times each team won the toss and also won the match'
            },
            xAxis: {
                categories: teams,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of times team won a match'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.f} times</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Total Win',
                data: timesTeamWon,
                colorByPoint: true,
            }],
        });
    });

// player who has won the highest number of Player of the Match awards for each season

fetch("output/6-player-won-highest-player-of-the-match-each-season.json")
    .then((data) => data.json())
    .then((data) => {
        // console.log(data);

        let countSeasons = 0;

        const allManOfTheMatch = Object.values(data)
            .reduce((teamsData, currentTeam) => {
                const playerPresent = teamsData.find((player) => {
                    return player.name === currentTeam.name;
                });

                if (playerPresent === undefined) {

                    let winData = Array.apply(null, Array(countSeasons))
                        .map(() => {
                            return null
                        });

                    winData.push(Number(currentTeam.won));

                    teamsData.push({
                        name: currentTeam.name,
                        data: winData
                    });
                } else {
                    playerPresent.data.push(Number(currentTeam.won));
                }

                countSeasons += 1;

                return teamsData;
            }, []);

        Highcharts.chart('container6', {
            chart: {
                type: 'area'
            },
            title: {
                text: 'player who has won the highest number of Player of the Match awards for each season',
            },
            yAxis: {
                title: {
                    useHTML: true,
                    text: 'man of the matches'
                }
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><br>'
            },
            plotOptions: {
                series: {
                    pointStart: 2008
                },
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: allManOfTheMatch
        });
    });

// find the strike rate of a batsman for each season

fetch("output/7-strike-rate-of-batsman-each-season.json")
    .then((data) => data.json())
    .then((data) => {
        // console.log(data);

        const allStrikeRates = Object.values(data)
            .reduce((strikeRates, currentSeason) => {
                let allSeason = Object.keys(currentSeason)
                    .map((season) => {
                        return season;
                    })
                    .filter((season) => {
                        const seasonNotInList = strikeRates.find((currentSeason) => {
                            return currentSeason === season;
                        });
                        return seasonNotInList === undefined;

                    });

                strikeRates.push(...allSeason);

                return strikeRates;
            }, []);

        allStrikeRates.sort();

        let playerCount = 1;
        const strikeRatePerSeason = Object.values(data)
            .reduce((batsmenStrikeRate, currentPlayerInfo) => {
                Object.entries(currentPlayerInfo)
                    .map(([season, strikeRate]) => {
                        const currentPlayerDetails = batsmenStrikeRate.find((currentPlayerInfo) => {
                            return currentPlayerInfo.name === season;
                        });
                        if (currentPlayerDetails === undefined) {
                            batsmenStrikeRate.push({
                                name: season,
                                data: [Number(strikeRate)]
                            });
                        } else {
                            currentPlayerDetails.data.push(Number(strikeRate));
                        }
                    });
                allStrikeRates.map((currentSeason) => {
                    const batsmanData = batsmenStrikeRate.find((batsmanInfo) => {
                        return batsmanInfo.name === currentSeason;
                    });
                    if (batsmanData === undefined) {
                        batsmenStrikeRate.push({
                            name: currentSeason,
                            data: [null],
                        });
                    } else {
                        if (batsmanData.data.length !== playerCount) {
                            batsmanData.data.push(null);
                        }
                    }
                });
                playerCount += 1;

                return batsmenStrikeRate;
            }, []);

        const batsmanAsCategories = Object.keys(data);

        // console.log(strikeRatePerSeason);

        Highcharts.chart('container7', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Strike rate of a batsman for each season'
            },
            xAxis: {
                categories: batsmanAsCategories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Strike rate'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.f} strike rate</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: strikeRatePerSeason
        });

    });

// Highest number of times one player has been dismissed by another player

fetch("output/8-highest-times-one-player-dismissed-another-player.json")
    .then((data) => data.json())
    .then((data) => {

        // console.log(data);

        const showData = [data];

        Highcharts.chart('container8', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Highest number of times one player has been dismissed by another player'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: ': <b>{point.y:.1f} </b>'
            },
            series: [{
                name: 'Population',
                data: showData,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center',
                    format: '{point.y:.f}', // one decimal
                    y: 20, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });

// Bowler with the best economy in super overs

fetch("output/9-best-economy-bowler-in-super-overs.json")
    .then((data) => data.json())
    .then((data) => {

        // console.log(data);

        const showData = [
            [data.name, Number(data.economy)]
        ];

        Highcharts.chart('container9', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Highest number of times one player has been dismissed by another player'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: ': <b>{point.y:.1f} </b>'
            },
            series: [{
                name: "Bowler's Economy",
                data: showData,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center',
                    format: '{point.y:.1f}', // one decimal
                    y: 20, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });