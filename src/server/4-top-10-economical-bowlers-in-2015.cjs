const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function top10EconomicalBowler() {

    const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for the matches.csv
    const deliveriesFilePath = __dirname + "/../data/deliveries.csv"; // Path for the deliveries.csv
    const resultsFilePath = __dirname + "/../public/output/4-top-10-economical-bowlers-in-2015.json"; // Path for the output file

    const matches = [];
    const year = "2015";

    // Reading the matches.csv file using csvParser and storing it into matches

    fs.createReadStream(matchesFilePath)
        .pipe(csvParser())
        .on("data", (data) => matches.push(data))
        .on("end", () => {
            // storing all match Ids played in year 2015.

            const matchesIds = matches.filter((match) => {
                return match.season === year;
            })
                .map((match) => {
                    return match.id;
                });

            const deliveries = [];
            // Reading the deliveries.csv file using csvParser and storing it into deliveries

            fs.createReadStream(deliveriesFilePath)
                .pipe(csvParser())
                .on("data", (data) => deliveries.push(data))
                .on("end", () => {

                    // storing name, total runs, total balls, economy of a bowler in year 2015

                    const bowlersDetails = deliveries.filter((delivery) => {
                        return matchesIds.includes(delivery.match_id);
                    })
                        .reduce((bowlersList, currentDelivery) => {
                            const bowlerName = currentDelivery.bowler;
                            const ball = (currentDelivery.wide_runs === "0" && currentDelivery.noball_runs === "0") ? 1 : 0;
                            const currentRuns = Number(currentDelivery.total_runs) - (Number(currentDelivery.bye_runs) + Number(currentDelivery.legbye_runs) + Number(currentDelivery.penalty_runs));

                            const bowler = bowlersList.find((bowler) => {
                                return bowler.name === bowlerName;
                            });

                            if (bowler === undefined) {
                                bowlersList.push({
                                    name: bowlerName,
                                    totalRuns: currentRuns,
                                    totalBalls: ball,
                                    economy: (this.totalRuns / (this.totalBalls / 6)).toFixed(2)
                                });
                            } else {
                                bowler.totalRuns += currentRuns;
                                bowler.totalBalls += ball;
                                bowler.economy = (bowler.totalRuns / (bowler.totalBalls / 6)).toFixed(2);
                            }
                            return bowlersList;
                        }, []);

                    const top10EconomicalBowlers = bowlersDetails.sort((bowler1, bowler2) => {
                        return bowler1.economy - bowler2.economy;
                    }).slice(0, 10);

                    // converting the results to a string

                    const jsonString = JSON.stringify(top10EconomicalBowlers);

                    // writing the output into 4-top-10-economical-bowlers-in-2015.json

                    fs.writeFile(resultsFilePath, jsonString, (error) => {
                        if (error) {
                            console.log("Error writing file", error);
                        } else {
                            console.log("Successfully wrote file");
                        }
                    });

                    // console.log(top10EconomicalBowlers);
                });
        });
}

top10EconomicalBowler();