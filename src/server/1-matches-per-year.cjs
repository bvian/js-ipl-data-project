const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module


function matchesPerYear() {

  const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for matches.csv
  const resultsFilePath = __dirname + "/../public/output/1-matches-per-year.json"; // Path for the output file

  const matches = [];

  // Reading the matches.csv file using csvParser and storing it into matches

  fs.createReadStream(matchesFilePath)
    .pipe(csvParser())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      // storing the number of matches played per year for all the years in IPL.

      const matchesPlayedPerYearResult = matches.reduce(
        (matchesPlayedPerYear, currentMatch) => {
          if (matchesPlayedPerYear[currentMatch.season] === undefined) {
            matchesPlayedPerYear[currentMatch.season] = 1;
          } else {
            matchesPlayedPerYear[currentMatch.season] += 1;
          }

          return matchesPlayedPerYear;
        }, {});

      // converting the results to a string

      const jsonString = JSON.stringify(matchesPlayedPerYearResult);

      //writing the output into 1-matches-per-year.json file

      fs.writeFile(resultsFilePath, jsonString, (error) => {
        if (error) {
          console.log("Error writing file", error);
        } else {
          console.log("Successfully wrote file");
        }
      });

      // console.log(matches);
    });
}

matchesPerYear();
