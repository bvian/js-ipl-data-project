const csv = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function matchesWonPerTeam() {

  const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for the matches.csv
  const resultsFilePath = __dirname + "/../public/output/2-matches-won-per-team-per-year.json"; // Path for the output file

  const matches = [];

  // Reading the matches.csv file using csvParser and storing it into matches

  fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      // storing the number of matches won per team per year in IPL.

      const matchesWonPerTeamPerYearResult = matches.reduce(
        (matchesWonPerTeamPerYear, currentMatch) => {
          const year = currentMatch.season;
          const winnerTeam = currentMatch.winner;

          if (matchesWonPerTeamPerYear[year] === undefined) {
            matchesWonPerTeamPerYear[year] = {};
          }

          if (winnerTeam !== "") {
            if (matchesWonPerTeamPerYear[year][winnerTeam] === undefined) {
              matchesWonPerTeamPerYear[year][winnerTeam] = 1;
            } else {
              matchesWonPerTeamPerYear[year][winnerTeam] += 1;
            }
          }

          return matchesWonPerTeamPerYear;
        }, {});

      // converting the results to a string

      const jsonString = JSON.stringify(matchesWonPerTeamPerYearResult);

      // writing the output into 2-matches-won-per-team-per-year.json file

      fs.writeFile(resultsFilePath, jsonString, (error) => {
        if (error) {
          console.log("Error writing file", error);
        } else {
          console.log("Successfully wrote file");
        }
      });

      // console.log(matchesWonPerTeamPerYearResult);
    });
}

matchesWonPerTeam();