const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module


function extraRunsConcededPerTeam() {

  const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for the matches.csv
  const deliveriesFilePath = __dirname + "/../data/deliveries.csv"; // Path for the deliveries.csv
  const resultsFilePath = __dirname + "/../public/output/3-extra-runs-conceded-per-team-in-2016.json"; // Path for the output file

  const matches = [];
  const year = "2016";

  // Reading the matches.csv file using csvParser and storing it into matches

  fs.createReadStream(matchesFilePath)
    .pipe(csvParser())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      // storing all match Ids played in year 2016.

      const matchesIds = matches.filter((match) => {
        return match.season === year;
      })
        .map((match) => {
          return match.id;
        });

      const deliveries = [];

      // Reading the deliveries.csv file using csvParser and storing it into deliveries

      fs.createReadStream(deliveriesFilePath)
        .pipe(csvParser())
        .on("data", (data) => deliveries.push(data))
        .on("end", () => {
          // storing extra runs conceded per team in year 2016

          const extraRunsPerYearResult = deliveries.filter((delivery) => {
            return matchesIds.includes(delivery.match_id);
          })
            .reduce((extraRunsPerYear, currentDelivery) => {
              const currentTeam = currentDelivery.bowling_team;
              const currentExtraRuns = Number(currentDelivery.extra_runs);

              if (extraRunsPerYear[currentTeam] === undefined) {
                extraRunsPerYear[currentTeam] = currentExtraRuns;
              } else {
                extraRunsPerYear[currentTeam] += currentExtraRuns;
              }
              return extraRunsPerYear;
            }, {});

          // converting the results to a string

          const jsonString = JSON.stringify(extraRunsPerYearResult);

          // writing the output into 3-extra-runs-conceded-per-team-in-2016.json file

          fs.writeFile(resultsFilePath, jsonString, (error) => {
            if (error) {
              console.log("Error writing file", error);
            } else {
              console.log("Successfully wrote file");
            }
          });

          // console.log(deliveries);
        });
    });
}

extraRunsConcededPerTeam();