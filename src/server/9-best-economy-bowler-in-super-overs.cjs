const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function bestEconomyBowlerInSuperOvers() {

    const deliveriesFilePath = __dirname + "/../data/deliveries.csv"; // Path for the deliveries.csv
    const resultsFilePath = __dirname + "/../public/output/9-best-economy-bowler-in-super-overs.json"; // Path for the output file

    const deliveries = [];

    // Reading the deliveries.csv file using csvParser and storing it into deliveries

    fs.createReadStream(deliveriesFilePath)
        .pipe(csvParser())
        .on("data", (data) => deliveries.push(data))
        .on("end", () => {

            // storing list of bowler's runs, balls and economy

            const bowlersInSuperOvers = deliveries.filter((currentDelivery) => {
                return currentDelivery.is_super_over !== "0";
            })
                .reduce((bowlersList, currentDelivery) => {
                    const bowlerName = currentDelivery.bowler;
                    const ball = (currentDelivery.wide_runs === "0" && currentDelivery.noball_runs === "0") ? 1 : 0;
                    const currentRuns = Number(currentDelivery.total_runs) - (Number(currentDelivery.bye_runs) + Number(currentDelivery.legbye_runs) + Number(currentDelivery.penalty_runs));

                    const bowler = bowlersList.find((bowler) => {
                        return bowler.name === bowlerName;
                    });

                    if (bowler === undefined) {
                        bowlersList.push({
                            name: bowlerName,
                            totalRuns: currentRuns,
                            totalBalls: ball,
                            economy: (this.totalRuns / (this.totalBalls / 6)).toFixed(2)
                        });
                    } else {
                        bowler.totalRuns += currentRuns;
                        bowler.totalBalls += ball;
                        bowler.economy = (bowler.totalRuns / (bowler.totalBalls / 6)).toFixed(2);
                    }
                    return bowlersList;
                }, []);

            // sorting bowler's data according to their economy in increasing order

            const topEconomicalBowlersInSuperOvers = bowlersInSuperOvers.sort((bowler1, bowler2) => {
                return bowler1.economy - bowler2.economy;
            });

            const jsonString = JSON.stringify(topEconomicalBowlersInSuperOvers[0]);

            // writing the output into 9-best-economy-bowler-in-super-overs.json file

            fs.writeFile(resultsFilePath, jsonString, (error) => {
                if (error) {
                    console.log("Error writing file", error);
                } else {
                    console.log("Successfully wrote file");
                }
            });

        });

}

bestEconomyBowlerInSuperOvers();