const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function totalTeamWonTossWonMatch() {

    const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for matches.csv
    const resultsFilePath = __dirname + "/../public/output/5-team-won-toss-also-won-match.json"; // Path for the output file

    const matches = [];

    // Reading the matches.csv file using csvParser and storing it into matches

    fs.createReadStream(matchesFilePath)
        .pipe(csvParser())
        .on("data", (data) => matches.push(data))
        .on("end", () => {

            // storing number of teams won the toss and also won match

            const teamWonMatchAndToss = matches.filter((match) => {
                return match.winner === match.toss_winner;
            })
                .reduce((acc, currentMatch) => {
                    if (acc[currentMatch.winner] === undefined) {
                        acc[currentMatch.winner] = 1;
                    } else {
                        acc[currentMatch.winner] += 1;
                    }
                    return acc;
                }, {});


            // converting the results to a string

            const jsonString = JSON.stringify(teamWonMatchAndToss);

            //writing the output into 5-team-won-toss-also-won-match.json file

            fs.writeFile(resultsFilePath, jsonString, (error) => {
                if (error) {
                    console.log("Error writing file", error);
                } else {
                    console.log("Successfully wrote file");
                }
            });

            // console.log(teamWonMatchAndToss);
        });
}

totalTeamWonTossWonMatch();