const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function strikeRateOfBatsmanEachSeason() {

    const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for the matches.csv
    const deliveriesFilePath = __dirname + "/../data/deliveries.csv"; // Path for the deliveries.csv
    const resultsFilePath = __dirname + "/../public/output/7-strike-rate-of-batsman-each-season.json"; // Path for the output file

    const matches = [];

    fs.createReadStream(matchesFilePath)
        .pipe(csvParser())
        .on("data", (data) => matches.push(data))
        .on("end", () => {

            //storing matches ID's of all season

            const matchesIds = matches.reduce((matchesId, currentMatch) => {

                matchesId[currentMatch.id] = currentMatch.season;

                return matchesId;
            }, {});

            // console.log(matchesIds);

            const deliveries = [];

            // Reading the deliveries.csv file using csvParser and storing it into deliveries

            fs.createReadStream(deliveriesFilePath)
                .pipe(csvParser())
                .on("data", (data) => deliveries.push(data))
                .on("end", () => {

                    // storing batsman total runs, total balls, and total strike rate

                    const batsmanDetailsEachSeason = deliveries.reduce((batsmanEachSeason, currentDelivery) => {

                        const runs = currentDelivery.batsman_runs;
                        const balls = (Number(currentDelivery.wide_runs) > 0) ? 0 : 1;
                        const batsman = currentDelivery.batsman;
                        const year = matchesIds[currentDelivery.match_id];

                        if (batsmanEachSeason[batsman] === undefined) {
                            batsmanEachSeason[batsman] = {};
                        }
                        if (batsmanEachSeason[batsman][year] === undefined) {
                            batsmanEachSeason[batsman][year] = {
                                runs: Number(runs),
                                balls: balls,
                                strikeRate: ((runs / balls) * 100).toFixed(2)
                            };
                        } else {
                            batsmanEachSeason[batsman][year].runs += Number(runs);
                            batsmanEachSeason[batsman][year].balls += balls;
                            batsmanEachSeason[batsman][year].strikeRate = ((batsmanEachSeason[batsman][year].runs / batsmanEachSeason[batsman][year].balls) * 100).toFixed(2);
                        }

                        return batsmanEachSeason;

                    }, {});

                    const strikeRateOfBatsman = {};

                    for (const [batsman, detailsEachSeason] of Object.entries(batsmanDetailsEachSeason)) {
                        strikeRateOfBatsman[batsman] = {};
                        for (const [year, batsmanStats] of Object.entries(detailsEachSeason)) {
                            strikeRateOfBatsman[batsman][year] = batsmanStats.strikeRate;
                        }
                    }

                    // converting the results to a string

                    const jsonString = JSON.stringify(strikeRateOfBatsman);

                    //writing the output into 7-strike-rate-of-batsman-each-season.json file

                    fs.writeFile(resultsFilePath, jsonString, (error) => {
                        if (error) {
                            console.log("Error writing file", error);
                        } else {
                            console.log("Successfully wrote file");
                        }
                    });

                    // console.log(strikeRateOfBatsman);

                });
        });
}

strikeRateOfBatsmanEachSeason();