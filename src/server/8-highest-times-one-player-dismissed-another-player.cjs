const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function highestTimesOnePlayerDismissedAnotherPlayer() {

    const deliveriesFilePath = __dirname + "/../data/deliveries.csv"; // Path for the deliveries.csv
    const resultsFilePath = __dirname + "/../public/output/8-highest-times-one-player-dismissed-another-player.json"; // Path for the output file

    const deliveries = [];

    // Reading the deliveries.csv file using csvParser and storing it into deliveries

    fs.createReadStream(deliveriesFilePath)
        .pipe(csvParser())
        .on("data", (data) => deliveries.push(data))
        .on("end", () => {

            // storing total times batsman dismissed by bowler pair 

            const batsmanDismissedBowler = deliveries.filter((delivery) => {
                if (delivery.dismissal_kind !== "run out" && delivery.batsman === delivery.player_dismissed) {
                    return true;
                } else {
                    return false;
                }
            })
                .reduce((batsmanDismissedRecords, delivery) => {
                    const batsmanBowlerPair = `${delivery.batsman} dismissed by ${delivery.bowler}`;
                    if (batsmanDismissedRecords[batsmanBowlerPair] === undefined) {
                        batsmanDismissedRecords[batsmanBowlerPair] = 1;
                    }
                    else {
                        batsmanDismissedRecords[batsmanBowlerPair] += 1;
                    }
                    return batsmanDismissedRecords;
                }, {});

            // sorting and storing the batsman dismissed bowler list in descending order.

            const batsmanDismissedBowlerList = Object.entries(batsmanDismissedBowler).sort((playerDismissed1, playerDismissed2) => {
                return playerDismissed2[1] - playerDismissed1[1];
            });

            const jsonString = JSON.stringify(batsmanDismissedBowlerList[0]);

            // writing the output into 8-highest-times-one-player-dismissed-another-player.json file

            fs.writeFile(resultsFilePath, jsonString, (error) => {
                if (error) {
                    console.log("Error writing file", error);
                } else {
                    console.log("Successfully wrote file");
                }
            });

            // console.log(batsmanDismissedBowlerList[0]);

        });

}

highestTimesOnePlayerDismissedAnotherPlayer();